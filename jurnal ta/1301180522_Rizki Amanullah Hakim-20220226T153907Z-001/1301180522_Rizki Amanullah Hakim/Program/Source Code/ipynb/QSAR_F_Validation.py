
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# In[ ]:


def q2_score(y, y_pred, y_train=None):
    sse = 0
    sst = 0
    if y_train is None:
        y_mean = np.mean(y)
    else:
        y_mean = np.mean(y_train)
    for i in range(len(y)):
        sse += (y[i] - y_pred[i]) ** 2
        sst += (y[i] - y_mean) ** 2
    q2_score = 1 - (sse / sst)
    return q2_score


def normalize(X):
    max_ = np.max(X, axis=0)
    min_ = np.min(X, axis=0)
    X_norm = (X - min_) / (max_ - min_)
    return max_, min_, X_norm


def qsar_param(y, y_pred, d_r2m=True):
    results = []
    _, _, y = normalize(y)
    _, _, y_pred = normalize(y_pred)
    y_mean = np.mean(y)
    y_pred_mean = np.mean(y_pred)
    # calculate r2
    num = 0
    den_1 = 0
    den_2 = 0
    for i in range(len(y)):
        num += (y[i] - y_mean) * (y_pred[i] - y_pred_mean)
        den_1 += (y_pred[i] - y_pred_mean) ** 2
        den_2 += (y[i] - y_mean) ** 2
    r2 = num ** 2 / (den_1 * den_2)
    results.append(r2)
    # calculate k and k_dash
    n_data = len(y)
    dot_ = 0
    y_pred2 = 0
    y2 = 0
    for i in range(n_data):
        dot_ += (y[i] * y_pred[i])
        y_pred2 += y_pred[i] ** 2
        y2 += y[i] ** 2
    k = np.sum(dot_) / np.sum(y_pred2)
    k_dash = np.sum(dot_) / np.sum(y2)
    results.append(k)
    results.append(k_dash)
    # calculate r2_0 and r2_0_dash
    num = 0
    num_dash = 0
    den = 0
    den_dash = 0
    for i in range(n_data):
        num += (y[i] - (k * y_pred[i])) ** 2
        num_dash += (y_pred[i] - (k_dash * y[i])) ** 2
        den += (y[i] - y_mean) ** 2
        den_dash += (y_pred[i] - y_pred_mean) ** 2
    r2_0 = 1 - (num / den)
    r2_0_dash = 1 - (num_dash / den_dash)
    results.append(r2_0)
    results.append(r2_0_dash)
    # calculate rm2 and rm2_dash
    rm2 = r2 * (1 - np.sqrt(r2 - r2_0))
    rm2_dash = r2 * (1 - np.sqrt(r2 - r2_0_dash))
    results.append(rm2)
    results.append(rm2_dash)
    # calculate rm2_bar and d_rm2
    rm2_bar = (rm2 + rm2_dash) / 2
    d_rm2 = np.abs(rm2 - rm2_dash)
    results.append(rm2_bar)
    results.append(d_rm2)
    r2r0 = (r2 - r2_0)/r2
    r2r0_dash = (r2 - r2_0_dash)/r2
    results.append(r2r0)
    results.append(r2r0_dash)
    r0r0_dash = np.abs(r2_0 - r2_0_dash)
    results.append(r0r0_dash)
    return results


def standardize(X):
    mean_ = np.mean(X, axis=0)
    std_ = np.std(X, axis=0)
    X_norm = (X - mean_) / std_
    return X_norm, mean_, std_


def mse_score(y, y_pred):
    if isinstance(y, np.ndarray) or isinstance(y, list):
        n_data = len(y)
        sum_err = 0
        for i in range(n_data):
            err = y[i] - y_pred[i]
            sum_err += (err ** 2)
        return sum_err / n_data
    else:
        return (y - y_pred) ** 2


def leverage(XtX, X):
    levs = []
    for i in range(X.shape[0]):
        x = X.iloc[i, :]
        lev = x.dot(XtX).dot(x.T)
        levs.append(lev)
    return levs


def applicability_domain(X_train, X_test, y_train_act,
                         y_train_pred, y_test_act, y_test_pred):
    # using wilson map
    X_train, _, _ = standardize(X_train)
    X_test, _, _ = standardize(X_test)
    n, p = X_train.shape
    # calculate standardized residuals
    err_train = []
    for i in range(len(y_train_pred)):
        err_train.append(y_train_act[i] - y_train_pred[i])
    rmse_train = np.sqrt(mse_score(y_train_act, y_train_pred))
    res_train = [a/rmse_train for a in err_train]
    err_test = []
    for i in range(len(y_test_pred)):
        err_test.append(y_test_act[i] - y_test_pred[i])
    rmse_test = np.sqrt(mse_score(y_test_act, y_test_pred))
    res_test = [a/rmse_test for a in err_test]
    # calculate leverage
    XtX = X_train.T.dot(X_train)
    XtX = np.linalg.pinv(XtX)
    lev_train = leverage(XtX, X_train)
    print(len(lev_train))
    lev_test = leverage(XtX, X_test)
    h_star = (3 * (p + 1)) / n
    print("h*= {:.2f}".format(h_star))
    # plot train data
    plt.scatter(lev_train, res_train, c='b', marker='o', label='training data')
    plt.scatter(lev_test, res_test, c='r', marker='^', label='test data')
    plt.axhline(y=3, c='k', linewidth=0.8)
    plt.axhline(y=-3, c='k', linewidth=0.8)
    plt.axvline(x=h_star, c='k', linewidth=0.8)
    plt.xticks([0, 0.1, 0.2, 0.3, 0.4, 0.5, h_star, 0.6],
               [0, 0.1, 0.2, 0.3, 0.4, 0.5, "h$^*$", 0.6])
    plt.xlim(0, h_star + 0.1)
    plt.ylim(-4, 4)
    plt.xlabel('leverage')
    plt.ylabel('standardized residual')
    plt.legend(loc='upper right')
    plt.savefig('app_domain.png', format='png', dpi=1000, bbox_inches="tight")
    plt.show()
